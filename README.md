# Nutanix ABS Storage Class

Testing out the ABS Storage Class for now till the CSI version (req: k8s 1.11) is stable

`nutanix-csi-storage` in development here: [Nutanix Helm Repo](https://github.com/nutanix/helm)

FYI: Nutanix ABS StorageClass isn't developed enough to live outside 'default' namepsace

## TODO
* get Helm Repo working so we can pull this chart remotely

## Setup
1. Add helm repo
```bash
helm repo add vanderhoofen https://gitlab.com/vanderhoofen/helm
```

2. Install it
```bash
helm install \
  --namespace kube-system \
  --name vanderhoofen/nutanix-abs \
  --set prismEndPoint=192.168.250.55:9440 \
  --set dataServiceEndPoint=192.168.250.80:3260 \
  --set username=SERVICE_ACCOUNT \
  --set password=PASSWORD
```

